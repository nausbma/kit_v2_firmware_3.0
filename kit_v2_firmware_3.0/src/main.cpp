/*
  main.cpp - Firmware for KIT V2
  Created by Bram Naus, 27th of August, 2019.
*/
#include "Arduino.h"
#include <FastLED.h>
#include "TypeDefinitions.h"
#include "PinDef.h"
#include "Animation.h"
#include "AudioAmp.h"
#include "WavPlayer.h"
#include "RTC.h"
#include "DataLogger.h"

/*------------------------------------------------------//
  VARIABLES
//------------------------------------------------------*/
int animationQueuePos = 0;

CRGB leds[NUM_TEETH];
CRGB * LedsPointer = leds;

CRGB candleYellow = {255, 147, 41};

BrushingMethod clientMethodPref = electricBrushing;
AnimationType activeAnimationType;    // This setting will be derived based on clientMethodPref in 'initClientPref'
AnimationScope activeAnimationScope;  // This setting will be derived based on clientMethodPref in 'initClientPref'
uint16_t clientSpeedPref = 3000;      // Amount of milliseconds it takes for a single tooth to animate --> slow = 4000; normal is 3000; fast is 2300.
uint8_t clientVolumePref = 55;
CRGB clientColourPref = candleYellow;

State state = idle;
uint64_t lastStateChange = 0;

bool startButtonState = false;
bool stopButtonState = false;

/*------------------------------------------------------//
  DECLARATIONS
//------------------------------------------------------*/

/* A section is defined by starting point and length */
/* 
Normal direction 
*/
Section section1 = {0,5};
Section section2 = {5,6};
Section section3 = {11,5};
Section section4 = {16,-5};
Section section5 = {21,-6};
Section section6 = {27,-5};
Section upperJaw = {0,16};
Section lowerJaw = {16,-16};
Section mouth = {0,32};


/* Predefining empty animation slots */
Animation * brushingAnimation = nullptr;
Animation * animationLayer1 = nullptr;
  
 Section * animationQueue[NUM_ANIMATION_STEPS] = {
    &section6, &section5, &section4,  // Step 1 - 2 - 3
    &section1, &section2, &section3,  // Step 4 - 5 - 6
    &section6, &section5, &section4,  // Step 7 - 8 - 9
    &section1, &section2, &section3,  // Step 10 - 11 - 12
  };

/*------------------------------------------------------//
  METHODS
//------------------------------------------------------*/

void initClientPref(){
  if (clientMethodPref == electricBrushing){
    activeAnimationType = breathe;
    activeAnimationScope = granular;
  }
  else if(clientMethodPref == manualBrushing){
    activeAnimationType = bounce;
    activeAnimationScope = full;
  }
  else{
    activeAnimationType = breathe;
    activeAnimationScope = granular;
  } 
}

void initFastled() {
  FastLED.addLeds<WS2812, DATA_PIN, GRB>(leds, NUM_TEETH);
  pinMode(DATA_PIN, OUTPUT);
    for (uint8_t i = 0; i < NUM_TEETH; i++){
    leds[i] = CRGB::Black;
  }
}

void initButtons() {
  pinMode(BUTTON_1_PIN, INPUT);
  pinMode(BUTTON_2_PIN, INPUT);
}

void updateAnimation(Animation * myAnimationLayer){
  if(myAnimationLayer != nullptr){
    uint8_t startTooth = myAnimationLayer -> startTooth;
    int8_t numTeeth = myAnimationLayer -> numTeeth;

    myAnimationLayer -> update();

    for (uint8_t i = 0; i < abs(numTeeth); i++){
      leds[i+startTooth].r = myAnimationLayer -> sectionArr[i].r;
      leds[i+startTooth].g = myAnimationLayer -> sectionArr[i].g;
      leds[i+startTooth].b = myAnimationLayer -> sectionArr[i].b;
    }
  }
}

void updateAllAnimations(){
  for (uint8_t i = 0; i < NUM_TEETH; i++){
    leds[i] = CRGB::Black;
  }
  updateAnimation(brushingAnimation);
  updateAnimation(animationLayer1);
}

void setState(State newState) {
  state = newState;
  lastStateChange = millis();
  Serial.println("--- New State --- ");  

  switch (state) {
    case intro:
    { 
      Serial.println("State: Intro");
      delete animationLayer1;
      animationLayer1 = new Animation(&mouth, breathe, full, 16000, CRGB::Green);
      // The playback hasn't yet started so SD logging can be perfromed before this point
      writeData("intro");
      playFile("intro");
      break;
    }
    case brushing:
    { 
      Serial.println("State: Brushing");
      delete animationLayer1;
      animationLayer1 = nullptr;
      // Removed by Bram Naus - By removing the statement a new section animation is issued when returning from pause, restarting the brushing of the section. 
      //if(brushingAnimation == nullptr){
        delete brushingAnimation;
        brushingAnimation = new Animation(animationQueue[animationQueuePos], activeAnimationType, activeAnimationScope, clientSpeedPref, clientColourPref);

        String file = "FASE";
        String number = String(animationQueuePos+1);
        file.append(number);
        const char * completeFile = file.c_str();
        playFile(completeFile);

        Serial.print("Animation Queue: ");
        Serial.println(animationQueuePos + 1);
      //}
      break;
    }
    case paused:
    {
      Serial.println("State: Paused");
      if(brushingAnimation != nullptr){
      brushingAnimation -> pauseAnimation();
      }
      delete animationLayer1;
      animationLayer1 = new Animation(animationQueue[animationQueuePos], breathe, full, 360000, CRGB::YellowGreen);
      stopPlayback();
      // The playback has stopped so SD logging can be perfromed from this point

      break;
    }
    case resumed:
    {
      Serial.println("State: Resumed");
      if(brushingAnimation != nullptr){
        brushingAnimation -> resumeAnimation();
      }      
      break;
    }
    case stopped:
    { 
      Serial.println("State: Stopped");
      if(brushingAnimation != nullptr){
        brushingAnimation -> stopAnimation();
      }
      stopPlayback();
      // The playback has stopped so SD logging can be perfromed from this point      
      animationQueuePos = 0;
      break;

    }
    case done:
    {
      delete brushingAnimation;
      brushingAnimation = nullptr;
      Serial.println("State: Done");
      delete animationLayer1;
      animationLayer1 = new Animation(&mouth, breathe, full, 5000, CRGB::Green);
      playFile("OUTRO");
      animationQueuePos = 0;
      break;
    }
    case idle:
    {
      Serial.println("State: Idle");
      delete brushingAnimation;
      brushingAnimation = nullptr;
      delete animationLayer1;
      animationLayer1 = nullptr;
      break;
    }
    default:
    {
      break;
    }
    
  } 
}

void checkAnimationQueue(){      
  if(brushingAnimation -> isComplete == true){
    animationQueuePos++;
    if (animationQueuePos < NUM_ANIMATION_STEPS){
      delete animationLayer1;
      animationLayer1 = nullptr;
      brushingAnimation = new Animation(animationQueue[animationQueuePos], activeAnimationType, activeAnimationScope, clientSpeedPref, clientColourPref);

      String file = "FASE";
      String number = String(animationQueuePos+1);
      file.append(number);
      const char * completeFile = file.c_str();
      playFile(completeFile);
        
      Serial.print("Animation Queue: ");
      Serial.println(animationQueuePos + 1);
    }
    else {
      Serial.println("Animation Queue: Done");
      setState(done);

    }
  }
}

void checkState(){
  switch (state) {
    case intro:
      if (animationLayer1 -> isComplete == true) {
        Serial.println("Intro -> Brushing");
        setState(brushing);
        digitalClockDisplay(); 
      }
      break;
    case brushing:
      checkAnimationQueue();
      break;
    case paused:
      break;
    case resumed:
      //if (millis() - lastStateChange > 100) {
        Serial.println("Resumed -> Brushing");
        setState(brushing);
      //}
      break;
    case stopped:
      //if (millis() - lastStateChange > 1000) {
        Serial.println("Stopped -> Idle");
        setState(idle);
      //}
      break;
    case done:
      if (animationLayer1 -> isComplete == true) {
        Serial.println("Done -> Idle");
        setState(idle);
      }
      break;
    case idle:
      break;
    default:
      break;
  }
}

void checkButtonState(){
  startButtonState = digitalRead(BUTTON_2_PIN);
  stopButtonState = digitalRead(BUTTON_1_PIN);

  if (startButtonState){
    delay(500);
    Serial.println("Button: Start");
    switch (state)
    {
      case intro:
        Serial.println("Button: No Action");
        break;
      case brushing:
        setState(paused);
        break;
      case paused:
        setState(resumed);
        break;
      case resumed:
        setState(paused);
        break;
      case stopped:
        Serial.println("Button: No Action");
        break;
      case done:
        Serial.println("Button: No Action");
        break;
      case idle:
        Serial.println("Test");
        setState(intro);
        break;    
      default:
        break;
    }
  }  
  if (stopButtonState){
    delay(500);
    Serial.println("Button: Stopped");
    if(state != idle && state != stopped){    
       setState(stopped);    
    }
    else {
      Serial.println("Button: No Action");
    }
  }
}

/*------------------------------------------------------//
  SETUP & MAIN
//------------------------------------------------------*/

void setup() {
    Serial.begin(9600);
    delay(3000);
    Serial.println("Start of setup");
    initDataLogger();
    initRTC();
    initAudioAmp();
    initWavPlayer();
    initClientPref();
    initFastled();
    initButtons();
    Serial.println("End of setup");
}

void loop() {
  checkButtonState();
  checkState();
  updateAllAnimations();
  FastLED.show();
  if((millis() % 100) == 0){
    writeData("check");
  }
}
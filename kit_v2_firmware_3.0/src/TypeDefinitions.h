/*
  TypeDefinitions.h - Library handling all the global definitions.
  Created by Bram Naus, 27th of August, 2019.
*/

#ifndef TypeDefinitions_h
#define TypeDefinitions_h

#include "Arduino.h"

// typedef struct {
//   uint8_t r;
//   uint8_t g;
//   uint8_t b;
// } Colour;

typedef struct {
  uint8_t startTooth;
  int8_t numTeeth;
} Section;

enum AnimationType {
  none,
  bounce,
  breathe,
  fadeOut,
  fadeOn,
  pause,
};

enum AnimationScope {
  full,
  granular,
};

enum State   {
  intro,
  brushing,
  paused,
  resumed,
  stopped,
  done,
  idle,
};

enum BrushingMethod {
  electricBrushing,
  manualBrushing,
};

enum BrushingSpeed  {
  slow,
  normal,
  fast,
};

#endif

/*
  AudioAmp.cpp - Library handling playing audio files.
  Created by Bram Naus, 7th of March, 2019.
*/

#include "AudioAmp.h"

boolean setvolume(int8_t v) {
  // cant be higher than 63 or lower than 0
  if (v > 63) v = 63;
  if (v < 0) v = 0;
  
  Wire.beginTransmission(0x4b);
  Wire.write(v);
  if (Wire.endTransmission() == 0) 
    return true;
  else
    return false;
}

void initAudioAmp(){
    Wire.begin();
  
  if (! setvolume(42)) { //max volume is 50
    Serial.println("Failed to set volume, MAX9744 not found!");
    while (1);
  }
    pinMode(AMP_MUTE_PIN,OUTPUT); //PB03 is Mute for class d amp
    digitalWrite(AMP_MUTE_PIN,LOW);
}
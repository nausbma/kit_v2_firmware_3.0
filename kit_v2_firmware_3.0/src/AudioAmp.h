/*
  AudioAmp.h - Library handling all the global definitions.
  Created by Bram Naus, 7th of March, 2019.
*/

#ifndef AudioAmp_h
#define AudioAmp_h

#include "Arduino.h"
#include "PinDef.h"
#include <Wire.h>

boolean setvolume(int8_t v);
void initAudioAmp();

#endif
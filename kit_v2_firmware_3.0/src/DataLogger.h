/*
  DataLogger.h - Library handling the Data DataLogger.
  Created by Bram Naus, 14th of March, 2019.
*/

#ifndef DataLogger_h
#define DataLogger_h

#include <SD.h>
#include <SPI.h>
#include "RTC.h"
#include "PinDef.h"

void writeData(int data);
void writeData(String data);
void initDataLogger();


#endif
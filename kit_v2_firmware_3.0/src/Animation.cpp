/*
  Animation.cpp - Library handling all animations of sections.
  Created by Bram Naus, 27th of February, 2019.
*/

#include "Animation.h"

/*Animation constructor based on startTooth and numTeeth*/
Animation::Animation(uint8_t myStartTooth, int8_t myNumTeeth, AnimationType myAnimationType, AnimationScope myAnimationScope, uint32_t myDuration, CRGB myColour){
  animationType = myAnimationType;
  animationScope = myAnimationScope;
  colour = myColour;

  isComplete = false;
  isPaused = false;

  _startTime = millis();  
  _loopDuration = 1500;
  numTeeth = myNumTeeth;
  startTooth = myStartTooth;

  if(animationScope == full){
    _duration = myDuration;
    _timePerTooth = _duration / abs(numTeeth);
  }
  else if (animationScope == granular){
    _duration = myDuration * abs(numTeeth);
    _timePerTooth = myDuration;
  }

  updateMillisBetweenLeds();
  clearIntensity();
}

/* Animation constructor based on Section */
Animation::Animation(Section * mySectionPtr, AnimationType myAnimationType, AnimationScope myAnimationScope, uint32_t myDuration, CRGB myColour)
: Animation(mySectionPtr -> startTooth, mySectionPtr -> numTeeth, myAnimationType, myAnimationScope, myDuration, myColour){
}

/*Animation constructor for single tooth*/
Animation::Animation(uint8_t myStartTooth, uint32_t myDuration, CRGB myColour) 
: Animation(myStartTooth, 1, breathe, full, myDuration, myColour){
}

/* Basic constructor */
Animation::Animation() 
: Animation(0, 0, none, full, 1500, CRGB::Black){  
}

void Animation::setIntensity(uint8_t i, uint8_t myIntensity){
  sectionArr[i].r = int(colour.r*(myIntensity/255.0));
  sectionArr[i].g = int(colour.g*(myIntensity/255.0));
  sectionArr[i].b = int(colour.b*(myIntensity/255.0));
}

void Animation::clearIntensity(){
  for( int i = 0; i < abs(numTeeth); i++){
    setIntensity (i, 0);
  }
}

void Animation::updateDuration(uint32_t newDuration){
  _duration = newDuration;
}

void Animation::updateLoopDuration(uint32_t newLoopDuration){
  _loopDuration = newLoopDuration;
  updateMillisBetweenLeds();
}

void Animation::updateMillisBetweenLeds(){
  _millisBetweenLeds = (_loopDuration / (abs(numTeeth) + 1)) + 1;
}

void Animation::update() {
  if(animationType == none || isPaused == true){
    return;
  }

  _progression =  millis() - _startTime;
  _loopProgression = _progression % _loopDuration;
  _isUneven = (_progression / _loopDuration) % 2;

  if(_progression >= _duration){
    isComplete = true;
    return;
  }

  switch(animationType)
  {
      case breathe:
        updateAnimationBreathe();
        break;
      case bounce:
        updateAnimationBounce();
        break;
      default:
        Serial.println("Unknown animation type");
  }
}

void Animation::updateAnimationBreathe(){
  int lowIntensity = 55;
  int highIntensity = 200;
  int intensityDifference = highIntensity - lowIntensity;
  _activeLed = _progression / _timePerTooth;
  if(numTeeth < 0){
    _activeLed = abs(numTeeth) -1 - _activeLed;
  }
  
  if(_isUneven){
    clearIntensity();
    _intensity = Easing::easeInOutQuad(_loopProgression, lowIntensity, intensityDifference, _loopDuration);

    if(animationScope == full){
      for( int i = 0; i < abs(numTeeth); i++){
        setIntensity(i, _intensity);
      }
    }
    else if(animationScope == granular){
      setIntensity(_activeLed, _intensity);
    }
  }
  else {
    clearIntensity();
    _intensity = Easing::easeInOutQuad(_loopProgression, highIntensity, -intensityDifference, _loopDuration);
    
    if(animationScope == full){
      for( int i = 0; i < abs(numTeeth); i++){
        setIntensity(i, _intensity);
      }
    }
    else if(animationScope == granular){
      setIntensity(_activeLed, _intensity);
    }
  }
}

/* HAS NOT BEEN UPDATED FOR REVERSE ANIMATION (NEGATIVE NUMLED) */
void Animation::updateAnimationBounce(){
  clearIntensity();
  _position = Easing::easeInOutQuad(_loopProgression, 0, _loopDuration, _loopDuration);
  _interLedPos = _position % _millisBetweenLeds;
  _activeLed = _position / _millisBetweenLeds;
  _intensity = map(_interLedPos, 0, _millisBetweenLeds, 0, 255);
  _intensityRemainder = (255 - _intensity);

  if(_isUneven){
    if(_activeLed != numTeeth){
      setIntensity(_activeLed, _intensity);
    }
    if(_activeLed != 0 ){
      setIntensity(_activeLed - 1, _intensityRemainder);
    }
  }
  else{
    int reverseActiveLed = numTeeth - _activeLed;
    if(reverseActiveLed != numTeeth){
      setIntensity(reverseActiveLed, _intensityRemainder);
    }
    if(reverseActiveLed != 0 ){
      setIntensity(reverseActiveLed -1, _intensity);
    }
  }
}

void Animation::pauseAnimation(){
  isPaused = true;
}

void Animation::resumeAnimation(){
  isPaused = false;
  _startTime = millis() - _progression;  
}

void Animation::stopAnimation(){
  clearIntensity();
  isComplete = true;
}
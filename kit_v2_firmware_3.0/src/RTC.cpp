/*
  RTC.cpp - Library handling the Real Time Clock.
  Created by Bram Naus, 14th of March, 2019.
*/

#include "RTC.h"

#include <TimeLib.h>

#define TIME_HEADER  "T"

time_t getTeensy3Time(){
  return Teensy3Clock.get();
}

String printDigits(int digits){
  // utility function for digital clock display: prints preceding colon and leading 0
  String dataString = ":";
  if(digits < 10){
    dataString += '0';
  }
  dataString += digits;
  return dataString;
}

void serialDigitalClockDisplay() {
  // digital clock display of the time
  Serial.print(hour());
  String tempMinutes = printDigits(minute());
  Serial.print(tempMinutes);
  String tempSeconds = printDigits(second());
  Serial.print(tempSeconds);
  Serial.print(" ");
  Serial.print(day());
  Serial.print(" ");
  Serial.print(month());
  Serial.print(" ");
  Serial.print(year()); 
  Serial.println(); 
}

String digitalClockDisplay() {

  String myMinute = printDigits(minute());
  String mySecond = printDigits(second());

  String timeDate = hour() + myMinute + " " + day() + "/" + month() + "/" + year();
  return timeDate;
}

void initRTC(){
    // set the Time library to use Teensy 3.0's RTC to keep time
    setSyncProvider(getTeensy3Time);

    if (timeStatus()!= timeSet) {
        Serial.println("Unable to sync with the RTC");
    } else {
        Serial.println("RTC has set the system time");
    }
    digitalClockDisplay();  
}


/*
  PinDef.h - Library handling all the global definitions.
  Created by Bram Naus, 27th of August, 2019.
*/

#ifndef PinDef_h
#define PinDef_h

/*------------------------------------------------------//
  CONSTANTS || defined by KIT_V2_PCB design
//------------------------------------------------------*/
const int SD_DETECTOR_PIN = 5;

const int DATA_PIN = 7;

const int BUTTON_1_PIN = 8;
const int BUTTON_2_PIN = 9;

const int SD_CS_PIN = 10;     //DAT3 in KIT_V2_Base pcb
const int SD_MOSI_PIN = 11;   //CMD in KIT_V2_Base pcb
const int SD_DAT0_PIN = 12;
const int SD_SCLK_PIN = 13;

const int TEENSY_PROG_PIN = 15;

const int AMP_SDA_PIN = 18;
const int AMP_SCLK_PIN = 19;
const int AMP_ADDR1_PIN = 20;
const int AMP_ADDR2_PIN = 21;
const int AMP_MUTE_PIN = 22;
const int AMP_SHDN_PIN = 23;

const int NUM_TEETH = 32;
const int NUM_ANIMATION_STEPS = 12;

#endif
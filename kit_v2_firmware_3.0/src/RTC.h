/*
  RTC.h - Library handling the Real Time Clock.
  Created by Bram Naus, 14th of March, 2019.
*/

#ifndef RTC_h
#define RTC_h

#include "arduino.h"

time_t getTeensy3Time();
String printDigits(int digits);
void SerialDigitalClockDisplay();
String digitalClockDisplay();
void initRTC();

#endif
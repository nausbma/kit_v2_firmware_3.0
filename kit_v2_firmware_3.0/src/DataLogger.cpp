/*
  DataLogger.cpp - Library handling the Data DataLogger.
  Created by Bram Naus, 14th of March, 2019.
*/

#include "DataLogger.h"

File dataFile;

void initDataLogger(){
  Serial.print("Initializing SD card...");
  
//   // see if the card is present and can be initialized:
//   pinMode(10, OUTPUT);
//   SPI.setMOSI(7);
//   SPI.setSCK(14);
//   if (!SD.begin(10)) {
//     Serial.println("Card failed, or not present");
//     // don't do anything more:
//     return;
//   }
//   Serial.println("Card initialized.");

  dataFile = SD.open("datalog.txt", FILE_WRITE);    
}

void writeData(String data){
    String dataString = "";
    dataString += data;
    dataString += ",";
    dataString += digitalClockDisplay();

    dataFile = SD.open("datalog.txt", FILE_WRITE);    

    // if the file is available, write to it:
    if (dataFile) {
        dataFile.println(dataString);
        dataFile.close();
        // print to the serial port too:
        Serial.println(dataString);
    }  
    // if the file isn't open, pop up an error:
    else {
        Serial.println("error opening datalog.txt");
    } 
}
void writeData(int data){
    //String dataString = "";
    //dataString += data;

    File dataFile = SD.open("datalog.txt", FILE_WRITE);    

    // if the file is available, write to it:
    if (dataFile) {
        dataFile.println(data);
        dataFile.close();
        // print to the serial port too:
        Serial.println(data);
    }  
    // if the file isn't open, pop up an error:
    else {
        Serial.println("error opening datalog.txt");
    } 
}
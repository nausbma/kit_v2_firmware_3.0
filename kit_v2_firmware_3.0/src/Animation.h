/*
  Animation.h - Library handling all animations of sections.
  Created by Bram Naus, 27th of August, 2019.
*/

#ifndef Animation_h
#define Animation_h

#include "Arduino.h"
#include "TypeDefinitions.h"
#include <FastLED.h>
#include <Easing.h>

class Animation 
{  
  public:
  Animation(uint8_t, int8_t, AnimationType, AnimationScope, uint32_t, CRGB);
  Animation(Section *, AnimationType, AnimationScope, uint32_t, CRGB);
  Animation(uint8_t, uint32_t, CRGB);
  Animation();  

  void updateMillisBetweenLeds();
  void updateDuration(uint32_t);
  void updateLoopDuration(uint32_t);

  void update();
  void updateAnimationBreathe();
  void updateAnimationBounce();
  void pauseAnimation();
  void resumeAnimation();
  void stopAnimation(); // finish animation for loop duration but not entire duration. Making sure an animation ends gracefully. 

  Section * sectionPointer;
  uint8_t startTooth;
  int8_t numTeeth;
  AnimationType animationType;
  AnimationScope animationScope;
  CRGB colour;

  bool isComplete;
  bool isPaused;
  CRGB sectionArr[32];
      
  private:
  void clearIntensity();
  void setIntensity(uint8_t, uint8_t);
  bool _isUneven;

  uint32_t _startTime; 
  uint32_t _millisBetweenLeds;

  uint32_t _timePerTooth;
  uint32_t _duration;
  uint32_t _progression;

  uint32_t _loopDuration;
  uint32_t _loopProgression;

  uint8_t _position;
  uint8_t _interLedPos;
  uint8_t _activeLed;
  uint8_t _intensity;
  uint8_t _intensityRemainder;   
};
#endif
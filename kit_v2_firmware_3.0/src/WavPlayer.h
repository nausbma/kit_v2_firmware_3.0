/*
  WavPlayer.h - Library handling all the global definitions.
  Created by Bram Naus, 7th of March, 2019.
*/

#ifndef WavPlayer_h
#define WavPlayer_h

#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>
#include <string>
#include "PinDef.h"

void initWavPlayer();
void playFile(const char *filename);
void stopPlayback();

#endif

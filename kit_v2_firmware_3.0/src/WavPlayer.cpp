/*
  WavPlayer.cpp - Library handling playing audio files.
  Created by Bram Naus, 7th of March, 2019.
*/

#include "WavPlayer.h"

AudioPlaySdWav       audioTrack;
AudioOutputAnalog    audioOutput;
AudioConnection      patchCord1(audioTrack, 0, audioOutput, 0);
AudioConnection      patchCord2(audioTrack, 1, audioOutput, 1);

void initWavPlayer() {
  AudioMemory(8);
  SPI.setMOSI(SD_MOSI_PIN);
  SPI.setSCK(SD_SCLK_PIN);
  if (!(SD.begin(SD_CS_PIN))) {
    // stop here, but print a message repetitively
    while (1) {
      Serial.println("Unable to access the SD card");
      delay(500);
    }
  }
}

void playFile(const char *filename){
    if(audioTrack.isActive()){
        audioTrack.stop();
        Serial.print("stopped audio track");
    }
    
    String file = filename;
    String fileExtension = ".WAV";
    file.append(fileExtension);
    const char * completeFile = file.c_str();

    Serial.print("Playing file: ");
    Serial.println(completeFile);

    audioTrack.play(completeFile);

    // A brief delay for the library read WAV info
    delay(5);
}

void stopPlayback(){
    audioTrack.stop();
}